# PX4 Vehicle Status Notification Documentation

Provides Visual and Audible notification of **Vehicle Status** and **Readiness to Fly**

Examples of Notification:
- Whether if the vehicle is properly calibrated
- Has SD Card
- If its in position lock 
- Safe for approach
- Whether it is armed

## LED Meaning

### UI LED 


[Solid Blue] Armed, No GPS Lock

Indicates vehicle has been armed and has no position lock from a GPS unit.
PX4 will unlock control of the motors, allowing you to fly your drone


[Pulsing Blue] Disarmed, No GPS Lock

When disarmed, the motors cant be controlled, but other systems are active

[Solid Green] Armed, GPS Lock

PX4 will unlock control of the motors, allowing you to fly your drone.
In this mode, vehicle can perform guided missions.

[Pulsing Green] Disarmed, GPS Lock

This means you will not be able to control motors, but all other subsystems including GPS position lock are working


[Solid Purple] Failsafe Mode

 This mode will activate whenever vehicle encounters an issue during flight, such as losing manual control, a critically low battery, or an internal error.

[Solid Amber] Low Battery Warning


[Blinking Red] Error / Setup Required



### Status LED

## Tune Meaning

## Preflight Sensor

